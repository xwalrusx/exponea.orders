import assert from 'assert';

import OrderTransform from './orderTransform';

function test(rows, expected) {
	rows.unshift('header');
	expected = expected.map(str => str+'\n');
	const result = [];

	const transform = new OrderTransform({ dontEncodeEmail: true });
	transform.push = str => result.push(str);

	rows.forEach(row => transform._transform(row, 'string', () => { }))
	transform._flush(() => {});

	assert.deepEqual(result, expected);
}

describe('orderTransform', () => {
    it('empty list', ()=> {
    	test([], []);
    });

    it('single item order', ()=> {
    	test(
    		[ 'email1|order1|item1|100|order' ],
    		[ 'email1,order1,[item1],100' ]
    	);
    });

    it('single item refund', ()=> {
    	test(
    		[ 'email1|order1|item1|100|refund' ],
    		[ 'email1,order1,[item1],-100' ]
    	);
    });

    it('multi item order', ()=> {
    	test([
    		'email1|order1|item1|100|order',
    		'email1|order1|item2|50|order',
    		'email1|order1|item3|25|order'
		], [
			'email1,order1,[item1, item2, item3],175'
		]);
    });

    it('multi item refund', ()=> {
    	test([
    		'email1|order1|item1|100|refund',
    		'email1|order1|item2|50|refund',
    		'email1|order1|item3|25|refund'
    	], [
    		'email1,order1,[item1, item2, item3],-175'
    	]);
    });

    it('multi user orders', ()=> {
    	test([
    		'email1|order1|item1|100|order',
    		'email1|order1|item2|50|order',
    		'email1|order1|item3|25|order',
    		'email1|order2|item1|100|order',
    		'email1|order2|item2|50|order',
    		'email2|order1|item1|10|order',
    		'email2|order2|item1|25|order',
    		'email2|order2|item2|50|order',
    		'email2|order2|item3|75|order'
		], [
			'email1,order1,[item1, item2, item3],175',
			'email1,order2,[item1, item2],150',
			'email2,order1,[item1],10',
			'email2,order2,[item1, item2, item3],150'
		]);
    });
});
