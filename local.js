import fs from 'fs';
import { LineTransform, LineFilter } from 'node-line-reader';

import OrderTransform from './orderTransform';

const FILE = './orders.small.csv';

fs.createReadStream(FILE)
  	.pipe(new LineTransform())
  	.pipe(new LineFilter({ skipEmpty: true }))
  	.pipe(new OrderTransform())
  	.pipe(process.stdout);
