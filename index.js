import express from 'express';
import axios from 'axios';
import { LineTransform, LineFilter } from 'node-line-reader';

import OrderTransform from './orderTransform';

const app = express()
const port = 3000
const fileUrl = `https://storage.googleapis.com/exp-framework.appspot.com/orders.csv`

app.get('/load', (request, response) => {
    axios({
        method: 'get',
        url: fileUrl,
        responseType:'stream'
    }).then(res => {
        /* DO NOT MODIFY ABOVE */

        /* INSERT YOUR CODE HERE */
        const error = handleError.bind(null, response);
        
        res.data.on('error', error)
            .pipe(new LineTransform()).on('error', error)
            .pipe(new LineFilter({ skipEmpty: true })).on('error', error)
            .pipe(new OrderTransform()).on('error', error)
            .pipe(response).on('error', error);

		/* DO NOT MODIFY BELOW */
    }).catch(err => console.log(err));
})

app.listen(port, () => console.log(`App listening on port http://localhost:${port}`))

function handleError(response, err) {
    response.status(500).end();
    console.log(err);
}
