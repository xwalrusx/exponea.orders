const EMAIL = 0;
const ORDER_ID = 1;
const ITEM_ID = 2;
const PRICE = 3;
const ACTION = 4;
const ACTION_PLUS = 'order';
const ACTION_MINUS = 'refund';

export default class Order {
	constructor(row) {
		this.email = row[ EMAIL ];
		this.order_id = row[ ORDER_ID ];
		this.items = [ ];
	    this.total = 0;

	    this.addItem(row);
	}

	isEqual(row) {
	    return row[ EMAIL ] === this.email && row[ ORDER_ID ] === this.order_id;
	}

	addItem(row) {
	    const itemId = row[ ITEM_ID ];
	    this.items.push(itemId);

	    const action = row[ ACTION ];
	    const price = parseInt(row[ PRICE ]);

	    switch(action) {
	    	case ACTION_PLUS:
	    		this.total += price;
	    		break;
	    	case ACTION_MINUS:
		    	this.total -= price;
		    	break;
	    }
	}

	toString(options) {
		return [
			options && options.dontEncodeEmail ? this.email : base64(this.email),
			this.order_id,
			'[' + this.items.join(', ') + ']',
			this.total
		].join(',')
		+ '\n'
	}
}

function base64(str) {
	return Buffer.from(str).toString('base64');
}
