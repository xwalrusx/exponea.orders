import { Transform } from 'stream';
import { StringDecoder } from 'string_decoder';

import Order from './order';

const SEP = '|';

export default class OrderTransform extends Transform {
    constructor(options){
        super(options);
        this._options = options;
        this._decoder = new StringDecoder(options && options.defaultEncoding);
        this._order = null;
        this._headerConsumed = false;
    }
  
    _transform(chunk, enc, done) {
        if(enc === 'buffer') {
            chunk = this._decoder.write(chunk);
        }

        if(!this._headerConsumed) {
            this._headerConsumed = true;
            done();
        } else {
            const row = chunk.split(SEP);
            this._consumeRow(row);
            done();
        }
    };

    _flush(done) {
        if(this._order) {
            this.push(this._order.toString(this._options));
        }
        done();
    }

    _consumeRow(row) {
        if(this._order) {
            if(this._order.isEqual(row)) {
                this._order.addItem(row);
            } else {
                this.push(this._order.toString(this._options));
                this._order = new Order(row);
            }
        } else {
            this._order = new Order(row);
        }
    }
}
